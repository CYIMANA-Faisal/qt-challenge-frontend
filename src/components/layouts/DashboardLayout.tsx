import React, { ReactNode } from "react";
import SideNav from "../Dashboard/SideNav";

interface DashboardLayoutProps {
  children: ReactNode;
}

const DashboardLayout: React.FC<DashboardLayoutProps> = ({ children }) => {
  return (
    <div className="flex h-screen flex-col md:flex-row md:overflow-hidden">
      <div className="w-full flex-none md:w-60 bg-slate-900">
        <SideNav />
      </div>
      <div className="flex-grow p-6 md:overflow-y-auto md:p-12 bg-slate-100">
        {children}
      </div>
    </div>
  );
};

export default DashboardLayout;
