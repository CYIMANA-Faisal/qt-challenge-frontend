import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface Task {
  id: number;
  title: string;
  startDate: string;
  endDate: string;
  assignees: string[];
  collaborators: string[];
  project: string;
  description: string;
  priority: string;
  attachment: string;
  savedAs: string;
}

const initialState: Task[] = [];

export const tasksSlice = createSlice({
  name: "tasks",
  initialState,
  reducers: {
    setTask: (state, action: PayloadAction<Task>) => {
      state.push(action.payload);
    },
  },
});

export const { setTask } = tasksSlice.actions;

export default tasksSlice.reducer;
