import { Power } from "lucide-react";
import NavLinks from "./NavLinks";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { clearAllListeners } from "@reduxjs/toolkit";

export default function SideNav() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(clearAllListeners());
    navigate("/");
  };

  return (
    <div className="flex h-full flex-col px-3 py-4 md:px-2">
      <div className="flex grow flex-row justify-between space-x-2 md:flex-col md:space-x-0 md:space-y-2">
        <div className="hidden h-auto w-full grow rounded-md md:block">
          <NavLinks />
        </div>
        <form onSubmit={handleLogout}>
          <button
            type="submit"
            className="flex h-[48px] w-full grow items-center justify-center gap-2 text-slate-200 rounded-md p-2 text-sm font-medium hover:bg-slate-200 hover:text-slate-900 md:flex-none md:justify-start md:p-2 md:px-3"
          >
            <Power className="w-6" />
            <div className="hidden md:block">Sign Out</div>
          </button>
        </form>
      </div>
    </div>
  );
}
