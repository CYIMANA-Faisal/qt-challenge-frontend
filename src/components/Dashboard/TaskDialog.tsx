import { useState, useEffect } from "react";
import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { cn } from "@/lib/utils";
import { Calendar } from "@/components/ui/calendar";
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from "@/components/ui/popover";
import { format } from "date-fns";
import { CalendarIcon } from "lucide-react";
import * as React from "react";

import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";

import {
  useUpdateTaskMutation,
  useDeleteTaskMutation,
  useCreateTaskMutation,
} from "../../features/api/endpoints/tasks";

export const TaskDialog = ({ task, mode, onSave, onClose }) => {
  const [startDate, setStartDate] = useState<Date>();
  const [endDate, setEndDate] = useState<Date>();
  const [currentTask, setCurrentTask] = useState({
    title: "",
    startDate: "",
    endDate: "",
    assignees: [],
    collaborators: [],
    project: "",
    description: "",
    priority: "",
    attachment: "",
    savedAs: "",
  });

  useEffect(() => {
    if (task) {
      setCurrentTask(task);
      setStartDate(new Date(task.startDate));
      setEndDate(new Date(task.endDate));
    }
  }, [task]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setCurrentTask((prevTask) => ({
      ...prevTask,
      [name]:
        name === "assignees" || name === "collaborators"
          ? value.split(",")
          : value,
    }));
  };

  const [createtask, { isLoading, isError, error }] = useCreateTaskMutation();
  const [updateTask] = useUpdateTaskMutation();

  const handleSave = async () => {
    const taskData = {
      ...currentTask,
      startDate: startDate ? startDate.toISOString() : "",
      endDate: endDate ? endDate.toISOString() : "",
    };

    try {
      if (mode === "add") {
        // Create a new task
        const response = await createtask(taskData).unwrap();
        console.log("Task added successfully", response);
      } else if (mode === "edit") {
        // Update an existing task
        const response = await updateTask(taskData).unwrap();
        console.log("Task updated successfully", response);
      }

      onSave(taskData); // Call onSave to notify the parent component
    } catch (error) {
      console.error(
        mode === "add" ? "Failed to add task:" : "Failed to update task:",
        error
      );
    }
  };

  const dialogTitle =
    mode === "add" ? "Add Task" : mode === "edit" ? "Edit Task" : "View Task";
  const isReadOnly = mode === "view";

  return (
    <Dialog open={true} onOpenChange={onClose}>
      <DialogContent className="sm:max-w-[75%]">
        <DialogHeader>
          <DialogTitle>{dialogTitle}</DialogTitle>
          <DialogDescription>
            {mode === "add"
              ? "Fill in the details for the new task."
              : mode === "edit"
              ? "Make changes to the task here. Click save when you're done."
              : "View the details of the task."}
          </DialogDescription>
        </DialogHeader>
        <div className="grid gap-4 py-4">
          <div className="grid grid-cols-4 items-center gap-4">
            <Label htmlFor="title" className="text-right">
              Title
            </Label>
            <Input
              id="title"
              name="title"
              value={currentTask.title}
              onChange={handleChange}
              className="col-span-3"
              readOnly={isReadOnly}
            />
          </div>
          <div className="grid grid-cols-4 items-center gap-4">
            <Label htmlFor="startDate" className="text-right">
              Start Date
            </Label>
            <Popover>
              <PopoverTrigger asChild>
                <Button
                  variant={"outline"}
                  className={cn(
                    "w-[240px] justify-start text-left font-normal",
                    !startDate && "text-muted-foreground"
                  )}
                >
                  <CalendarIcon className="mr-2 h-4 w-4" />
                  {startDate ? (
                    format(startDate, "PPP")
                  ) : (
                    <span>Pick a date</span>
                  )}
                </Button>
              </PopoverTrigger>
              <PopoverContent className="w-auto p-0" align="start">
                <Calendar
                  mode="single"
                  selected={startDate}
                  onSelect={setStartDate}
                  initialFocus
                />
              </PopoverContent>
            </Popover>
          </div>
          <div className="grid grid-cols-4 items-center gap-4">
            <Label htmlFor="endDate" className="text-right">
              End Date
            </Label>
            <Popover>
              <PopoverTrigger asChild>
                <Button
                  variant={"outline"}
                  className={cn(
                    "w-[240px] justify-start text-left font-normal",
                    !endDate && "text-muted-foreground"
                  )}
                >
                  <CalendarIcon className="mr-2 h-4 w-4" />
                  {endDate ? format(endDate, "PPP") : <span>Pick a date</span>}
                </Button>
              </PopoverTrigger>
              <PopoverContent className="w-auto p-0" align="start">
                <Calendar
                  mode="single"
                  selected={endDate}
                  onSelect={setEndDate}
                  initialFocus
                />
              </PopoverContent>
            </Popover>
          </div>
          <div className="grid grid-cols-4 items-center gap-4">
            <Label htmlFor="assignees" className="text-right">
              Assignees
            </Label>
            <Input
              id="assignees"
              name="assignees"
              value={currentTask.assignees.join(",")}
              onChange={handleChange}
              className="col-span-3"
              readOnly={isReadOnly}
            />
          </div>
          <div className="grid grid-cols-4 items-center gap-4">
            <Label htmlFor="collaborators" className="text-right">
              Collaborators
            </Label>
            <Input
              id="collaborators"
              name="collaborators"
              value={currentTask.collaborators.join(",")}
              onChange={handleChange}
              className="col-span-3"
              readOnly={isReadOnly}
            />
          </div>
          <div className="grid grid-cols-4 items-center gap-4">
            <Label htmlFor="project" className="text-right">
              Project
            </Label>
            <select
              name="project"
              onChange={handleChange}
              className="w-[180px] border border-slate-200 rounded-sm py-3 px-3"
            >
              <option value="">Projects</option>
              <option value="project_a">Project A</option>
              <option value="project_b">Project B</option>
              <option value="project_c">Project C</option>
              <option value="project_d">Project D</option>
              <option value="project_e">Project E</option>
            </select>
          </div>
          <div className="grid grid-cols-4 items-center gap-4">
            <Label htmlFor="description" className="text-right">
              Description
            </Label>
            <Input
              id="description"
              name="description"
              value={currentTask.description}
              onChange={handleChange}
              className="col-span-3"
              readOnly={isReadOnly}
            />
          </div>
          <div className="grid grid-cols-4 items-center gap-4">
            <Label htmlFor="priority" className="text-right">
              Priority
            </Label>
            <select
              name="priority"
              onChange={handleChange}
              className="w-[180px] border border-slate-200 rounded-sm py-3 px-3"
            >
              <option value="">Select Priority</option>
              <option value="low">Low</option>
              <option value="normal">Normal</option>
              <option value="high">High</option>
            </select>
          </div>

          <div className="grid grid-cols-4 items-center gap-4">
            <Label htmlFor="attachment" className="text-right">
              Attachment
            </Label>
            <Input
              id="attachment"
              name="attachment"
              value={currentTask.attachment}
              onChange={handleChange}
              className="col-span-3"
              readOnly={isReadOnly}
            />
          </div>
          <div className="grid grid-cols-4 items-center gap-4">
            <Label htmlFor="savedAs" className="text-right">
              Saved As
            </Label>
            <select
              id="savedAs"
              name="savedAs"
              value={currentTask.savedAs}
              onChange={handleChange}
              className="w-[180px] border border-slate-200 rounded-sm py-3 px-3"
            >
              <option value="draft">Draft</option>
              <option value="final">Final</option>
            </select>
          </div>
        </div>
        {mode !== "view" && (
          <DialogFooter>
            <Button type="button" onClick={handleSave}>
              Save
            </Button>
          </DialogFooter>
        )}
      </DialogContent>
    </Dialog>
  );
};
