import React from "react";
import DashboardLayout from "../components/layouts/DashboardLayout";

const ProfilePage: React.FC = () => {
  // Mock profile data for testing
  const profile = {
    displayName: "John Doe",
    bio: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    profession: "Software Engineer",
    location: "New York, USA",
    gender: "Male",
    levelOfEducation: "Graduate",
  };

  return (
    <DashboardLayout>
      <div className="max-w-3xl mx-auto p-6">
        <h1 className="text-3xl font-bold mb-4">My Profile</h1>
        <div className="bg-white shadow-md rounded-lg p-6 mb-8">
          <div className="mb-4">
            <h2 className="text-xl font-semibold mb-2">Display Name</h2>
            <p>{profile.displayName}</p>
          </div>
          <div className="mb-4">
            <h2 className="text-xl font-semibold mb-2">Bio</h2>
            <p>{profile.bio}</p>
          </div>
          <div className="mb-4">
            <h2 className="text-xl font-semibold mb-2">Profession</h2>
            <p>{profile.profession}</p>
          </div>
          <div className="mb-4">
            <h2 className="text-xl font-semibold mb-2">Location</h2>
            <p>{profile.location}</p>
          </div>
          <div className="mb-4">
            <h2 className="text-xl font-semibold mb-2">Gender</h2>
            <p>{profile.gender}</p>
          </div>
          <div className="mb-4">
            <h2 className="text-xl font-semibold mb-2">Level of Education</h2>
            <p>{profile.levelOfEducation}</p>
          </div>
        </div>
      </div>
    </DashboardLayout>
  );
};

export default ProfilePage;
