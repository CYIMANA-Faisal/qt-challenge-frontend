import { createBrowserRouter } from "react-router-dom";
// import ProtectedRoute from "./utils/ProtectedRoutes";
import HomePage from "./pages/HomePage";
import DashboardPage from "./pages/DashboardPage";

const router = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />,
  },
  {
    path: "/dashboard",
    element: <DashboardPage />,
  },
]);

export default router;
