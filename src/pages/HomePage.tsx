import React, { ReactNode } from "react";

interface HomePageProps {
  children: ReactNode;
}

const HomePage: React.FC<HomePageProps> = ({ children }) => {
  return (
    <div className="w-full h-screen flex items-center justify-center bg-slate-100">
      {children}
    </div>
  );
};

export default HomePage;
