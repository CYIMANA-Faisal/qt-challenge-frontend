import LoginForm from "./components/home-page/login-form";
import RegisterForm from "./components/home-page/register-form";
import Dashboard from "./pages/DashboardPage";
import HomePage from "./pages/HomePage";
import { Route, Routes } from "react-router-dom";
import ProfilePage from "./pages/ProfilesPage";
import ChangePassword from "./pages/ChangePassword";
function App() {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <HomePage>
            <LoginForm />
          </HomePage>
        }
      />
      <Route
        path="/register"
        element={
          <HomePage>
            <RegisterForm />
          </HomePage>
        }
      />
      <Route path="/dashboard" element={<Dashboard />} />
      <Route path="/profile" element={<ProfilePage />} />
      <Route path="/change-password" element={<ChangePassword />} />
    </Routes>
  );
}

export default App;
