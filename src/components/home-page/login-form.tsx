import { CiLock } from "react-icons/ci";
import { useState } from "react";
import { useLoginMutation } from "../../features/api/endpoints/auth";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { AlertTriangle } from "lucide-react";
import { Alert, AlertDescription, AlertTitle } from "@/components/ui/alert";
import { useDispatch } from "react-redux";
import { setAuthenticatedUser } from "../../features/auth/authSlice";
import { Link, useNavigate } from "react-router-dom";

const LoginForm = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();

  const dispatch = useDispatch();

  const [login, { isLoading, isError, error }] = useLoginMutation();

  const loginUser = async () => {
    try {
      await login({ email, password })
        .unwrap()
        .then((response) => {
          console.log("Login successfully", response.payload);
          const { accessToken } = response.payload;
          dispatch(setAuthenticatedUser({ accessToken }));
          navigate("/dashboard");
        });
    } catch (error) {
      console.error("Failed to login:", error);
    }
  };

  const handleSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault();
    try {
      await loginUser();
    } catch (error) {
      console.log("Error", error);
    }
  };

  return (
    <div className="relative w-1/2 h-full flex flex-col items-center justify-center gap-y-12">
      <CiLock className="text-slate-600 w-20 h-20" />
      <h1 className="text-slate-700 text-3xl">Login</h1>
      <form className="flex flex-col gap-4 w-[60%]" onSubmit={handleSubmit}>
        <div>
          <Label
            htmlFor="email"
            className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
          >
            Email
          </Label>
          <Input
            id="email"
            type="text"
            placeholder="simpleuser@qtglobal.rw"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </div>
        <div className="mb-6">
          <Label
            htmlFor="password"
            className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
          >
            Password
          </Label>
          <Input
            id="password"
            type="password"
            placeholder="•••••••••"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            disabled={isLoading}
            required
          />
        </div>
        <div>
          <p>
            Dont have account? click here to{" "}
            <Link to="/register" className="text-sky-400">
              register
            </Link>
          </p>
        </div>
        <Button type="submit">{isLoading ? "Login..." : "Login"}</Button>
        {isError && (
          <Alert variant="destructive">
            <AlertTriangle className="h-4 w-4" />
            <AlertTitle className="font-bold">Error</AlertTitle>
            <AlertDescription className="font-semibold">
              {error.data.message[0]}
            </AlertDescription>
          </Alert>
        )}
      </form>
    </div>
  );
};

export default LoginForm;
