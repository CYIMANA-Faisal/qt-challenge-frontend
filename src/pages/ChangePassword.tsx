import React, { useState } from "react";
import { Button } from "../components/ui/button";
import DashboardLayout from "../components/layouts/DashboardLayout";
import {
  useChangePasswordMutation,
  useLoginMutation,
} from "../features/api/endpoints/auth";
import { useNavigate } from "react-router-dom";
import { AlertTriangle } from "lucide-react";
import { Alert, AlertDescription, AlertTitle } from "@/components/ui/alert";
import { useSelector } from "react-redux";

const ChangePassword: React.FC = () => {
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmNewPassword, setConfirmNewPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const navigate = useNavigate();

  const accessToken = useSelector((state) => state.auth.accessToken);

  const handleChangePassword = async () => {
    try {
      // Validate if new password matches confirm password
      if (newPassword !== confirmNewPassword) {
        setErrorMessage("New password and confirm password don't match");
        return;
      }

      // Construct the request body
      const requestBody = {
        oldPassword,
        newPassword,
      };

      // Construct the request headers
      const headers = {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`,
      };

      // Perform the fetch request
      const response = await fetch(
        "http://localhost:3000/api/v1/auth/change-password",
        {
          method: "PATCH",
          headers,
          body: JSON.stringify(requestBody),
        }
      );

      // Check if the response is OK
      if (!response.ok) {
        // Extract error message from response
        const errorData = await response.json();
        throw new Error(errorData.message);
      }

      // Handle successful password change
      console.log("Password changed successfully");
      // Navigate to a different page if needed
      navigate("/");
    } catch (error) {
      // Handle errors
      console.error("Failed to change password:", error);
      setErrorMessage(error.message);
    }
  };

  return (
    <DashboardLayout>
      <div className="max-w-md mx-auto mt-8 p-6 bg-slate-100 shadow-md rounded-lg">
        <h2 className="text-2xl font-bold mb-4">Change Password</h2>
        {errorMessage && (
          <div
            className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative mb-4"
            role="alert"
          >
            <strong className="font-bold">Error:</strong>
            <span className="block">{errorMessage}</span>
          </div>
        )}
        <div className="mb-4">
          <label
            htmlFor="oldPassword"
            className="block text-sm font-semibold mb-2"
          >
            Old Password
          </label>
          <input
            type="password"
            id="oldPassword"
            value={oldPassword}
            onChange={(e) => setOldPassword(e.target.value)}
            className="border border-gray-300 rounded px-3 py-2 w-full"
          />
        </div>
        <div className="mb-4">
          <label
            htmlFor="newPassword"
            className="block text-sm font-semibold mb-2"
          >
            New Password
          </label>
          <input
            type="password"
            id="newPassword"
            value={newPassword}
            onChange={(e) => setNewPassword(e.target.value)}
            className="border border-gray-300 rounded px-3 py-2 w-full"
          />
        </div>
        <div className="mb-6">
          <label
            htmlFor="confirmNewPassword"
            className="block text-sm font-semibold mb-2"
          >
            Confirm New Password
          </label>
          <input
            type="password"
            id="confirmNewPassword"
            value={confirmNewPassword}
            onChange={(e) => setConfirmNewPassword(e.target.value)}
            className="border border-gray-300 rounded px-3 py-2 w-full"
          />
        </div>
        <Button onClick={handleChangePassword}>Change Password</Button>
      </div>
    </DashboardLayout>
  );
};

export default ChangePassword;
