import React from "react";
import { Link } from "react-router-dom";
import { MessageCircleMore, Settings, Bolt } from "lucide-react";

interface NavLink {
  name: string;
  href: string;
  icon: React.ReactNode;
}

const links: NavLink[] = [
  { name: "Tasks", href: "/dashboard", icon: <MessageCircleMore /> },
  { name: "Profile", href: "/profile", icon: <Settings /> },
  { name: "Change Password", href: "/change-password", icon: <Settings /> },
];

const NavLinks: React.FC = () => {
  return (
    <>
      {links.map((link) => (
        <div className="flex h-[48px] w-full grow items-center justify-center gap-2 text-slate-200 rounded-md p-2 text-sm font-medium hover:bg-slate-200 hover:text-slate-900 md:flex-none md:justify-start md:p-2 md:px-3">
          {link.icon}
          <Link key={link.name} to={link.href}>
            {link.name}
          </Link>
        </div>
      ))}
    </>
  );
};

export default NavLinks;
