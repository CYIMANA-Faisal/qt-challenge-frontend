import React, { useState } from "react";
import DashboardLayout from "../components/layouts/DashboardLayout";
import {
  Table,
  TableBody,
  TableCaption,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from "@/components/ui/table";
import { CiMinimize1 } from "react-icons/ci";
import { Badge } from "@/components/ui/badge";
import { useDispatch } from "react-redux";
import {
  useGetTasksQuery,
  useDeleteTaskMutation,
} from "../features/api/endpoints/tasks";
import { Button } from "../components/ui/button";
import { TaskDialog } from "../components/Dashboard/TaskDialog";

const DashboardPage: React.FC = () => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [selectedTask, setSelectedTask] = useState(null);
  const [dialogMode, setDialogMode] = useState("add");

  const [deleteTask] = useDeleteTaskMutation();

  const handleOpenDialog = (task = null, mode = "add") => {
    setSelectedTask(task);
    setDialogMode(mode);
    setIsDialogOpen(true);
  };

  const handleCloseDialog = () => {
    setIsDialogOpen(false);
    refetch();
  };

  const handleSaveTask = (task) => {
    handleCloseDialog();
    refetch();
  };

  const dispatch = useDispatch();

  const handleDeleteTask = async (taskToDelete) => {
    try {
      const response = await deleteTask(taskToDelete.id).unwrap();
      refetch();
      console.log(response);
    } catch (error) {
      console.error("Failed to delete task:", error);
    }
  };

  const { data, error, isLoading, refetch } = useGetTasksQuery();

  if (isLoading) {
    return <p>Loading .........</p>;
  }

  return (
    <DashboardLayout>
      <Button
        className="border rounde-2 bg-slate-700"
        onClick={() => handleOpenDialog(null, "add")}
      >
        Add task
      </Button>
      <Table>
        <TableCaption>A list of all tasks.</TableCaption>
        <TableHeader>
          <TableRow>
            <TableHead className="w-[100px]">Title</TableHead>
            <TableHead>Start date</TableHead>
            <TableHead>End Date</TableHead>
            <TableHead>Assignees</TableHead>
            <TableHead>Collaborators</TableHead>
            <TableHead>Priority</TableHead>
            <TableHead>Actions</TableHead>
          </TableRow>
        </TableHeader>
        <TableBody>
          {data.payload.map((task) => (
            <TableRow key={task.id}>
              <TableCell>{task.title}</TableCell>
              <TableCell>{task.startDate}</TableCell>
              <TableCell>{task.endDate}</TableCell>
              <TableCell>
                {task.assignees.map((assignee) => (
                  <Badge key={assignee} className="bg-slate-700 mr-2 px-4 py-2">
                    {assignee}
                  </Badge>
                ))}
              </TableCell>
              <TableCell>
                {task.collaborators.map((collaborator) => (
                  <Badge
                    key={collaborator}
                    className="bg-slate-700 mr-2 px-4 py-2"
                  >
                    {collaborator}
                  </Badge>
                ))}
              </TableCell>
              <TableCell>
                <Badge className="bg-slate-400 px-4 py-2">
                  {task.priority}
                </Badge>
              </TableCell>
              <TableCell>
                <Button
                  className="border bg-slate-700 mr-2"
                  onClick={() => handleOpenDialog(task, "edit")}
                >
                  Edit
                </Button>
                <Button
                  className="border bg-green-400 mr-2"
                  onClick={() => handleOpenDialog(task, "view")}
                >
                  View
                </Button>
                <Button
                  className="border bg-red-400 mr-2"
                  onClick={() => handleDeleteTask(task)}
                >
                  Delete
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      {isDialogOpen && (
        <TaskDialog
          task={selectedTask}
          mode={dialogMode}
          onSave={handleSaveTask}
          onClose={handleCloseDialog}
        />
      )}
    </DashboardLayout>
  );
};

export default DashboardPage;
