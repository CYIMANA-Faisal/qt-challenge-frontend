import { CiMemoPad } from "react-icons/ci";
import { useState } from "react";
import { useRegisterMutation } from "../../features/api/endpoints/auth";
import { Label } from "@/components/ui/label";
import { Input } from "@/components/ui/input";
import { Button } from "@/components/ui/button";
import { AlertTriangle } from "lucide-react";
import { Alert, AlertDescription, AlertTitle } from "@/components/ui/alert";
import { Link, useNavigate } from "react-router-dom";

const RegisterForm = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  const [register, { isLoading, isError, error }] = useRegisterMutation();

  const createUser = async () => {
    try {
      await register({ name, email, password })
        .unwrap()
        .then((response) => {
          console.log("Register successfully", response.payload);
          navigate("/");
        });
    } catch (error) {
      console.error("Failed to register:", error);
    }
  };

  const handleSubmit = async (e: { preventDefault: () => void }) => {
    e.preventDefault();
    try {
      await createUser();
    } catch (error) {
      console.log("Error", error);
    }
  };

  return (
    <div className="relative w-1/2 h-full flex flex-col items-center justify-center gap-y-12">
      <CiMemoPad className="text-slate-600 w-20 h-20" />
      <h1 className="text-slate-700 text-3xl">Register</h1>
      <form className="flex flex-col gap-4 w-[60%]" onSubmit={handleSubmit}>
        <div>
          <Label
            htmlFor="name"
            className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
          >
            Full names
          </Label>
          <Input
            id="name"
            type="text"
            placeholder="John Doe"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
        </div>
        <div>
          <Label
            htmlFor="email"
            className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
          >
            Email
          </Label>
          <Input
            id="email"
            type="text"
            placeholder="simpleuser@qtglobal.rw"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </div>
        <div className="mb-6">
          <Label
            htmlFor="password"
            className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
          >
            Password
          </Label>
          <Input
            id="password"
            type="password"
            placeholder="•••••••••"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            disabled={isLoading}
            required
          />
        </div>
        <div>
          <p>
            Already have an account click here to{" "}
            <Link to="/" className="text-sky-400">
              Login
            </Link>
          </p>
        </div>
        <Button type="submit">{isLoading ? "Login..." : "Login"}</Button>
        {isError && (
          <Alert variant="destructive">
            <AlertTriangle className="h-4 w-4" />
            <AlertTitle className="font-bold">Error</AlertTitle>
            <AlertDescription className="font-semibold">
              {error.data.message[0]}
            </AlertDescription>
          </Alert>
        )}
      </form>
    </div>
  );
};

export default RegisterForm;
