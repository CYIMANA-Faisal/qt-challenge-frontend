import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface AuthState {
  accessToken: string | null;
}

const initialState: AuthState = {
  accessToken: null,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setAuthenticatedUser: (
      state,
      action: PayloadAction<{
        accessToken: string;
      }>
    ) => {
      const { accessToken } = action.payload;
      state.accessToken = accessToken;
    },
    clearAuthenticatedUser: (state) => {
      state.accessToken = null;
    },
  },
});

export const { setAuthenticatedUser, clearAuthenticatedUser } =
  authSlice.actions;

export default authSlice.reducer;
